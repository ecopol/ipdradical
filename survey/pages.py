from otree.api import Currency as c, currency_range

from ._builtin import Page, WaitPage
from .models import Constants

class Instructions(Page):    
    def is_displayed(self):
        return self.round_number == 1

    def before_next_page(self):
        participant = self.participant    
        participant.payoff = participant.payoff/self.session.config["IPD_payoff_multiplier"]

class survey(Page):
    form_model = 'player'
    form_fields = ['gender','age',
                   'work_type',
                   'perception_other_scrabble','perception_other_ipd','perception_me_scrabble','perception_me_ipd','politic','studies']

class survey2(Page):
    form_model = 'player'
    form_fields = ['chance','conservatism', 'equality', 'inferiors', 'antiautoritarian', 'equalitychance', 'equalitytreatment', 'racism']


page_sequence = [
    Instructions,
    survey,
    survey2,
]
