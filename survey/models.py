from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)
import random

from django import forms


class Constants(BaseConstants):
    name_in_url = 'survey'
    players_per_group = None
    num_rounds = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    
    age = models.IntegerField(
        label='Quelle est votre année de naissance ?',
        min = 1920, max = 2005
        )

    gender = models.StringField(
        choices=[
                ['h','un homme'],
                ['f','une femme']
                ],
        label='Vous êtes ?',
        widget=widgets.RadioSelectHorizontal
         )
    
    work_type = models.StringField(
        choices=[
                ['worker','Salarié'],
                ['auto','À votre compte'],
                ['edu','Étudiant'],
                ['no','Sans emploi'],
                ],
        label='Votre situation principale est actuellement :',
        widget=widgets.RadioSelectHorizontal
        )

    perception_other_scrabble = models.IntegerField(
        choices=[
                [3,'Très coopératifs'],
                [2,'Plutôt coopératifs'],
                [1,'Peu coopératifs'],
                [0,'Pas du tout coopératifs'],
                ],
        label='Pendant le jeu de création de mots, pouvez-vous nous indiquer si vous avez trouvé les autres joueurs :',
        widget=widgets.RadioSelectHorizontal
        )
    
    perception_other_ipd = models.IntegerField(
        choices=[
                [3,'Très coopératifs'],
                [2,'Plutôt coopératifs'],
                [1,'Peu coopératifs'],
                [0,'Pas du tout coopératifs'],
                ],
            label='Pendant le jeu économique de groupes, pouvez-vous nous indiquer si vous avez trouvé les autres joueurs :',
            widget=widgets.RadioSelectHorizontal
        )
    
    perception_me_scrabble = models.IntegerField(
        choices=[
                [3,'Très coopératifs'],
                [2,'Plutôt coopératifs'],
                [1,'Peu coopératifs'],
                [0,'Pas du tout coopératifs'],
                ],
        label='Pendant le jeu de création de mots, pouvez-vous nous indiquer si vous-même avez été :',
        widget=widgets.RadioSelectHorizontal
        )
    
    perception_me_ipd = models.IntegerField(
        choices=[
                [3,'Très coopératifs'],
                [2,'Plutôt coopératifs'],
                [1,'Peu coopératifs'],
                [0,'Pas du tout coopératifs'],
                ],
            label='Pendant le jeu économique de groupes, pouvez-vous nous indiquer si vous-même avez été :',
            widget=widgets.RadioSelectHorizontal
        )

    politic = models.IntegerField(
        choices=[
        [1, '1 (Gauche) '],
        [2, '2'],
        [3, '3'],
        [4, '4'],
        [5, '5'],
        [6, '6'],
        [7, '7'],
        [8, '8'],
        [9, '9'],
        [10, '10 (Droite)'],
    ],
        label='En politique, on classe habituellement les Français sur une échelle de ce genre qui va de la gauche à la droite. Vous personnellement, où vous classeriez-vous sur cette échelle ? ',
            widget=widgets.RadioSelectHorizontal
        )

    studies = models.IntegerField(
        choices=[
        [1, "aucun"],
        [2, "école primaire sans le CEP"],
        [3, "école primaire avec le CEP" ],
        [4, "collège (de la 6ème à la 3ème)"],
        [5, "enseignement professionnel après le collège SANS baccalauréat"],
        [6, "enseignement professionnel après le collège AVEC baccalauréat"],
        [7, "enseignement général des lycées (de la seconde à  la terminale) SANS baccalauréat"],
        [8, "enseignement général des lycées (de la seconde à la terminale) AVEC baccalauréat"],
        [9, "premier cycle universitaire (licence ou équivalent) SANS diplôme"],
        [10, "premier cycle universitaire (licence ou équivalent) AVEC diplôme"],
        [11, "deuxième cycle universitaire (master ou équivalent) SANS diplôme"],
        [12, "deuxième cycle universitaire (master ou équivalent) AVEC diplôme"],
        [13, "troisième cycle universitaire (doctorat) SANS diplôme"],
        [14, "troisième cycle universitaire (doctorat) AVEC diplôme"],
    ],
        label="Quel niveau d'études avez-vous atteint ?",
        )    
   
    chance = models.IntegerField(
        choices=[
        [-4, "-4"],
        [-3, "-3"],
        [-2, "-2"],
        [-1, "-1"],
        [0, "0"],
        [1, "+1"],
        [2, "+2"],
        [3, "+3"],
        [4, "+4"],
    ],
        label="Il est normal que certains groupes aient plus de chance dans la vie que d'autres.",
            widget=widgets.RadioSelectHorizontal

        ) 

    conservatism = models.IntegerField(
        choices=[
        [-4, "-4"],
        [-3, "-3"],
        [-2, "-2"],
        [-1, "-1"],
        [0, "0"],
        [1, "+1"],
        [2, "+2"],
        [3, "+3"],
        [4, "+4"],
    ],
        label="Il y aurait moins de problèmes si certains groupes acceptaient de rester à leur place.",
                    widget=widgets.RadioSelectHorizontal

        ) 

    equality = models.IntegerField(
        choices=[
        [-4, "-4"],
        [-3, "-3"],
        [-2, "-2"],
        [-1, "-1"],
        [0, "0"],
        [1, "+1"],
        [2, "+2"],
        [3, "+3"],
        [4, "+4"],
    ],
        label="Dans la mesure du possible, nous devons agir pour que les conditions des différents groupes soient égales.",
                   widget=widgets.RadioSelectHorizontal ) 

    inferiors = models.IntegerField(
        choices=[
        [-4, "-4"],
        [-3, "-3"],
        [-2, "-2"],
        [-1, "-1"],
        [0, "0"],
        [1, "+1"],
        [2, "+2"],
        [3, "+3"],
        [4, "+4"],
    ],
        label="Les groupes inférieurs devraient rester à leur place.",
                    widget=widgets.RadioSelectHorizontal

        ) 

    antiautoritarian = models.IntegerField(
        choices=[
        [-4, "-4"],
        [-3, "-3"],
        [-2, "-2"],
        [-1, "-1"],
        [0, "0"],
        [1, "+1"],
        [2, "+2"],
        [3, "+3"],
        [4, "+4"],
    ],
        label="Aucun groupe ne devrait dominer dans la société.",
                    widget=widgets.RadioSelectHorizontal

        ) 

    equalitychance = models.IntegerField(
        choices=[
        [-4, "-4"],
        [-3, "-3"],
        [-2, "-2"],
        [-1, "-1"],
        [0, "0"],
        [1, "+1"],
        [2, "+2"],
        [3, "+3"],
        [4, "+4"],
    ],
        label="On devrait accorder des chances égales dans la vie à tous les groupes.",
                    widget=widgets.RadioSelectHorizontal

        ) 

    equalitytreatment = models.IntegerField(
        choices=[
        [-4, "-4"],
        [-3, "-3"],
        [-2, "-2"],
        [-1, "-1"],
        [0, "0"],
        [1, "+1"],
        [2, "+2"],
        [3, "+3"],
        [4, "+4"],
    ],
        label="Il y aurait moins de problèmes si l'on traitait les gens de façon plus égalitaire.",
                    widget=widgets.RadioSelectHorizontal

        ) 

    racism = models.IntegerField(
        choices=[
        [-4, "-4"],
        [-3, "-3"],
        [-2, "-2"],
        [-1, "-1"],
        [0, "0"],
        [1, "+1"],
        [2, "+2"],
        [3, "+3"],
        [4, "+4"],
    ],
        label="Certains groupes de personnes sont tout simplement inférieurs aux autres groupes.",
                    widget=widgets.RadioSelectHorizontal

        ) 
