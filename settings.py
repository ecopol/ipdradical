from os import environ

# if you set a property in SESSION_CONFIG_DEFAULTS, it will be inherited by all configs
# in SESSION_CONFIGS, except those that explicitly override it.
# the session config can be accessed from methods in your apps as self.session.config,
# e.g. self.session.config['participation_fee']

SESSION_CONFIG_DEFAULTS = {
    'real_world_currency_per_point': 0.25,
    'participation_fee': 15.00,
    'doc': "",
}

SESSION_CONFIGS = [
#    {
#        'name': 'IPDradical_absolute_individualist',
#        'display_name': "IPDradical (absolute treatment) individualist",
#        'IPD_payoff_multiplier': 10,
#        'num_demo_participants': 15,
#        'app_sequence': ['IPDradical', 'payment_info'],
#        'standalone' : True,
#        'treatment':['','absolute','individualist'],
#        'use_browser_bots': False
#    },             
#    {
#        'name': 'IPS_vote_IPD_relative',
#        'display_name': "IPScrabble (vote) then IPDradical (relative)",
#        'standalone' : False,
#        'treatment':['vote','relative'],
#        'IPD_payoff_multiplier': 10,
#        'num_demo_participants': 15,
#        'app_sequence': ['IPScrabble', 'IPDradical', 'survey',  'payment_info'],
#    },  
#    {
#        'name': 'IPS_novote_IPD_relative',
#        'display_name': "IPScrabble (novote) then IPDradical (relative)",
#        'standalone' : False,
#        'treatment':['novote','relative'],
#        'IPD_payoff_multiplier': 10,
#        'num_demo_participants': 15,
#        'app_sequence': ['IPScrabble', 'IPDradical', 'survey', 'payment_info'],
#    },   
 {
        'name': 'IPS_vote_IPD_absolute_individualist',
        'display_name': "IPScrabble (vote) then IPDradical (absolute) individualist",
        'standalone' : False,
        'treatment':['vote','absolute','individualist'],
        'IPD_payoff_multiplier': 10,
        'num_demo_participants': 15,
        'app_sequence': ['IPScrabble', 'IPDradical', 'survey', 'payment_info'],
    },
#    {
#        'name': 'IPS_novote_IPD_absolute',
#        'display_name': "IPScrabble (novote) then IPDradical (absolute) parochial",
#        'standalone' : False,
#        'treatment':['novote','absolute','parochial'],
#        'IPD_payoff_multiplier': 10,
#        'num_demo_participants': 15,
#        'app_sequence': ['IPScrabble', 'IPDradical', 'survey', 'payment_info'],
#    },
#    {
#        'name': 'IPS_novote_IPD_absolute_individualist',
#        'display_name': "IPScrabble (novote) then IPDradical (absolute) individualist",
#        'standalone' : False,
#        'treatment':['novote','absolute','individualist'],
#        'IPD_payoff_multiplier': 10,
#        'num_demo_participants': 15,
#        'app_sequence': ['IPScrabble', 'IPDradical', 'survey', 'payment_info'],
#    },
]

ROOT_URLCONF = 'urls'

BROWSER_COMMAND = 'chromium'
# see the end of this file for the inactive session configs

# ISO-639 code
# for example: de, fr, ja, ko, zh-hans
LANGUAGE_CODE = 'fr'

# e.g. EUR, GBP, CNY, JPY
REAL_WORLD_CURRENCY_CODE = 'EUR'
USE_POINTS = True
POINTS_DECIMAL_PLACES = 1

ROOMS = [
    {
        'name': 'labo',
        'display_name': "Salle d'expérimentation du GAEL",
        'participant_label_file': '_rooms/liste_participants.txt',
        'use_secure_urls': False,
    },
]


# AUTH_LEVEL:
# this setting controls which parts of your site are freely accessible,
# and which are password protected:
# - If it's not set (the default), then the whole site is freely accessible.
# - If you are launching a study and want visitors to only be able to
#   play your app if you provided them with a start link, set it to STUDY.
# - If you would like to put your site online in public demo mode where
#   anybody can play a demo version of your game, but not access the rest
#   of the admin interface, set it to DEMO.

# for flexibility, you can set it in the environment variable OTREE_AUTH_LEVEL
AUTH_LEVEL = environ.get('OTREE_AUTH_LEVEL')

ADMIN_USERNAME = 'admin'
# for security, best to set admin password in an environment variable
ADMIN_PASSWORD = environ.get('OTREE_ADMIN_PASSWORD')

# Consider '', None, and '0' to be empty/false
DEBUG = (environ.get('OTREE_PRODUCTION') in {None, '', '0'})

DEMO_PAGE_INTRO_HTML = """
IPDradical : an inter-group social class prisoner dilemna.
"""

# don't share this with anybody.
SECRET_KEY = 'i#srww+p_v^(o^szimk6zn9oqiq)%mux03!9_0)%v9d!%(wz-#'

# if an app is included in SESSION_CONFIGS, you don't need to list it here
INSTALLED_APPS = ['otree']

# inactive session configs
#   {
#        'name': 'words_creation_task',
#        'display_name': "Words creation task",
#        'num_demo_participants': 1,
#        'app_sequence': ['words_task'],
#        'standalone' : True,
#        'IPD_payoff_multiplier': 10,
#        'treatment':['novote','absolute'],
#    }, 
#    {
#        'name': 'IPDradical_relative',
#        'display_name': "IPDradical (relative treatment)",
#        'IPD_payoff_multiplier': 10,
#        'num_demo_participants': 15,
#        'app_sequence': ['IPDradical', 'payment_info'],
#        'standalone' : True,
#        'treatment': ['','relative'],
#        'use_browser_bots': False
#    },
#    {
#        'name': 'IPDradical_absolute',
#        'display_name': "IPDradical (absolute treatment)",
#        'IPD_payoff_multiplier': 10,
#        'num_demo_participants': 15,
#        'standalone' : True,
#        'app_sequence': ['IPDradical', 'payment_info'],
#        'treatment': ['','absolute'],
#        'use_browser_bots': False
#    },
#        {
#        'name': 'IPScrabble_IP',
#        'display_name': "IPScrabble (IP treatment)",
#        'standalone' : True,
#        'treatment': 'IP',
#        'num_demo_participants': 3,
#        'app_sequence': ['IPScrabble', 'payment_info'],
#    },
#        {
#        'name': 'IPScrabble_noIP',
#        'display_name': "IPScrabble (noIP treatment)",
#        'standalone' : True,
#        'treatment': 'noIP',
#        'num_demo_participants': 3,
#        'app_sequence': ['IPScrabble', 'payment_info'],
#    },                
### {
###     'name': 'trust',
###     'display_name': "Trust Game",
###     'num_demo_participants': 2,
###     'app_sequence': ['trust', 'payment_info'],
### },
### {
###     'name': 'prisoner',
###     'display_name': "Prisoner's Dilemma",
###     'num_demo_participants': 2,
###     'app_sequence': ['prisoner', 'payment_info'],
### },
### {
###     'name': 'ultimatum',
###     'display_name': "Ultimatum (randomized: strategy vs. direct response)",
###     'num_demo_participants': 2,
###     'app_sequence': ['ultimatum', 'payment_info'],
### },
### {
###     'name': 'ultimatum_strategy',
###     'display_name': "Ultimatum (strategy method treatment)",
###     'num_demo_participants': 2,
###     'app_sequence': ['ultimatum', 'payment_info'],
###     'use_strategy_method': True,
### },
### {
###     'name': 'ultimatum_non_strategy',
###     'display_name': "Ultimatum (direct response treatment)",
###     'num_demo_participants': 2,
###     'app_sequence': ['ultimatum', 'payment_info'],
###     'use_strategy_method': False,
### },
### {
###     'name': 'vickrey_auction',
###     'display_name': "Vickrey Auction",
###     'num_demo_participants': 3,
###     'app_sequence': ['vickrey_auction', 'payment_info'],
### },
### {
###     'name': 'volunteer_dilemma',
###     'display_name': "Volunteer's Dilemma",
###     'num_demo_participants': 3,
###     'app_sequence': ['volunteer_dilemma', 'payment_info'],
### },
### {
###     'name': 'cournot',
###     'display_name': "Cournot Competition",
###     'num_demo_participants': 2,
###     'app_sequence': [
###         'cournot', 'payment_info'
###     ],
### },
### {
###     'name': 'principal_agent',
###     'display_name': "Principal Agent",
###     'num_demo_participants': 2,
###     'app_sequence': ['principal_agent', 'payment_info'],
### },
### {
###     'name': 'dictator',
###     'display_name': "Dictator Game",
###     'num_demo_participants': 2,
###     'app_sequence': ['dictator', 'payment_info'],
### },
### {
###     'name': 'matching_pennies',
###     'display_name': "Matching Pennies",
###     'num_demo_participants': 2,
###     'app_sequence': [
###         'matching_pennies',
###     ],
### },
### {
###     'name': 'traveler_dilemma',
###     'display_name': "Traveler's Dilemma",
###     'num_demo_participants': 2,
###     'app_sequence': ['traveler_dilemma', 'payment_info'],
### },
### {
###     'name': 'bargaining',
###     'display_name': "Bargaining Game",
###     'num_demo_participants': 2,
###     'app_sequence': ['bargaining', 'payment_info'],
### },
### {
###     'name': 'common_value_auction',
###     'display_name': "Common Value Auction",
###     'num_demo_participants': 3,
###     'app_sequence': ['common_value_auction', 'payment_info'],
### },
### {
###     'name': 'bertrand',
###     'display_name': "Bertrand Competition",
###     'num_demo_participants': 2,
###     'app_sequence': [
###         'bertrand', 'payment_info'
###     ],
### },
### {
###     'name': 'real_effort',
###     'display_name': "Real-effort transcription task",
###     'num_demo_participants': 1,
###     'app_sequence': [
###         'real_effort',
###     ],
### },
### {
###     'name': 'lemon_market',
###     'display_name': "Lemon Market Game",
###     'num_demo_participants': 3,
###     'app_sequence': [
###         'lemon_market', 'payment_info'
###     ],
### },
### {
###     'name': 'public_goods_simple',
###     'display_name': "Public Goods (simple version from tutorial)",
###     'num_demo_participants': 3,
###     'app_sequence': ['public_goods_simple', 'payment_info'],
### },
### {
###     'name': 'trust_simple',
###     'display_name': "Trust Game (simple version from tutorial)",
###     'num_demo_participants': 2,
###     'app_sequence': ['trust_simple'],
### },
