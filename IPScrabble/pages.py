from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants, Subsession

# In my_app.pages
from django.http import HttpResponse

def dictionary(request):
   
    word = request.POST.get('word')
    dictionary = open("IPScrabble/dic.txt").read().split()
    #Raises an error if the word being played is not in the official scrabble dictionary (dic.txt).
    if word in dictionary:
        exist = True
    else:
        exist = False
    return HttpResponse(exist)

class Instructions(Page):    
    def is_displayed(self):
        return self.round_number == 1

class Test(Page):
    timeout_seconds = Constants.test_timeout
    
    def is_displayed(self):
        return self.round_number == 1

    form_model = 'player'

    def get_form_fields(self):
        return ['test_words']     

    def vars_for_template(self):
        return {
            'test_hand': Constants.test_hand,
            'test_timeout' : int(Constants.test_timeout/60) #display in minutes
        }
        
    
class Board(Page):
    
    def get_timeout_seconds(self):
        if self.round_number/Constants.players_per_group <= Constants.board_timeout_bonus_rounds +  Constants.period_in_advance :
            return Constants.board_timeout + Constants.board_timeout_bonus + Constants.board_timeout_bonus_for_royalties
        else:
            return Constants.board_timeout + Constants.board_timeout_bonus_for_royalties
        
    timeout_submission = {'new_word': None, 'extended_word':None, 'letters_added':None,'royalties': 0}


    def is_displayed(self):
        if not self.session.vars['vote']:
            advance = self.round_number/Constants.players_per_group > Constants.period_in_advance
        else:
            advance = self.round_number/Constants.players_per_group > (2 * Constants.period_in_advance) + Constants.num_period_IP
            
        return ((self.round_number - 1) % Constants.players_per_group) + 1 == self.player.id_in_group and advance
    
    def before_next_page(self):
        self.player.set_board()
        self.player.set_hand()
        if self.timeout_happened:
            self.player.timeout = True
        
    def error_message(self, value):
        
        if value['new_word'] != None:
            if self.group.check_novelty(value['new_word']) == False:
                return "Le mot "+value['new_word']+" a déjà été créé"
            if self.subsession.check_dictionary(value['new_word']) == False:
                return "Le mot "+value['new_word']+" n'existe pas dans le dictionnaire"
            if value['extended_word'] == None :
                if len(value['new_word']) < Constants.min_nb_letters:
                    return "Le mot doit être composé de "+str(Constants.min_nb_letters)+" caractères minimum"
                if len(value['new_word']) > Constants.max_nb_letters:
                    return "Le mot doit être composé de "+str(Constants.max_nb_letters)+" caractères maximum"   
            else:
                if len(value['letters_added']) > Constants.ext_nb_letters:
                    return "Le mot doit être étendu de "+str(Constants.min_nb_letters)+" lettre au maximum"                
        
        if self.participant.vars['IPtreatment'] == 'IP':
            if value['royalties'] == None and value['letters_added'] != None :
                return "Vous devez indiquer une valeur pour les royalties (entre 0% et 100%)"
            if value['royalties'] != None and value['letters_added'] == None :
                return "Vous ne pouvez pas mettre de royalties si vous n'avez pas étendu ou créé un mot"

    
    form_model = 'player'

    def get_form_fields(self):
        if self.participant.vars['IPtreatment'] == 'IP':
            return ['new_word','extended_word','letters_added','royalties']
        else:
            return ['new_word','extended_word','letters_added'] 
     
    
    def vars_for_template(self):       
        
        self.player.hand = ''.join(self.player.participant.vars['hand'])
        self.player.myround = 1
        
        if self.participant.vars['IPtreatment'] == 'IP':
            royalties_earned = sum([p.royalties_earned for p in self.player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number))])
            royalties_spent = sum([p.royalties_spent for p in self.player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number))])
        else:
            royalties_earned = 0
            royalties_spent = 0         

        
        return {
            'subtreatment' : self.participant.vars['IPtreatment'],
            'round': self.round_number,
            'period': self.subsession.get_period(),
            'total_period': Constants.num_period_IP,
            'word_cost': self.player.get_board_word_cost(),
            'board': self.group.get_board_hist(),
            'new_letter': self.player.get_new_letter(),
            'hand': [[letter,Constants.letter_values[letter]] for letter in self.participant.vars['hand']],
            'royalties_earned': royalties_earned,
            'royalties_spent': -royalties_spent

        }

class Board_WaitPage(WaitPage):

    def is_displayed(self):
        if not self.session.vars['vote']:
            advance = self.round_number/Constants.players_per_group > Constants.period_in_advance
        else:
            advance = self.round_number/Constants.players_per_group > (2 * Constants.period_in_advance) + Constants.num_period_IP
            
        return advance
    
    template_name = 'IPScrabble/Board_WaitPage.html'
        

    def vars_for_template(self):

        if self.participant.vars['IPtreatment'] == 'IP':
            royalties_earned = sum([p.royalties_earned for p in self.player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number))])
            royalties_spent = sum([p.royalties_spent for p in self.player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number))])
        else:
            royalties_earned = 0
            royalties_spent = 0     

        return {
            'subtreatment' : self.participant.vars['IPtreatment'],
            'round': self.round_number,
            'period': self.subsession.get_period(),
            'total_period': Constants.num_period_IP,
            'word_cost': self.player.get_board_word_cost(),
            'new_letter': self.player.get_new_letter(),
            'hand': [[letter,Constants.letter_values[letter]] for letter in self.participant.vars['hand']],
            'royalties_earned': royalties_earned,
            'royalties_spent': -royalties_spent

        }   
    
    def after_all_players_arrive(self):
        pass


class Instructions_before_vote(Page):    
    def is_displayed(self):
        return self.round_number/Constants.players_per_group == (Constants.num_period_IP + Constants.period_in_advance)
  
    
class Vote(Page):
    
    form_model = 'player'
    form_fields = ['vote']
    
    def is_displayed(self):
        return self.round_number/Constants.players_per_group == (Constants.num_period_IP + Constants.period_in_advance) and self.session.config['treatment'][0] == 'vote'


    def vars_for_template(self):
        
        royalties_earned = sum([p.royalties_earned for p in self.player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number))])
        royalties_spent = sum([p.royalties_spent for p in self.player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number))])
      
        return {
         'subtreatment' : self.participant.vars['IPtreatment'],
            'royalties_earned': royalties_earned,
            'royalties_spent': -royalties_spent,
        }       

    
class ResultsVoteWaitPage(WaitPage):

    def is_displayed(self):
        return self.round_number/Constants.players_per_group == (Constants.num_period_IP + Constants.period_in_advance)
    
    title_text = "Veuillez patienter"
    body_text = "En attendant les résultats des autres participants"
    
    def after_all_players_arrive(self):      
        
        self.session.vars['subtreatment_round'] = self.round_number + 1
        
        self.group.vote_result()
        
        self.session.vars['vote'] = True
        
        self.group.reset_hand()
        
        self.group.reset_payoff()
    
    
class Test_WaitPage(WaitPage):

    def is_displayed(self):
        return self.round_number == 1
    
    title_text = "Veuillez patienter"
    body_text = "En attendant que les autres participants finissent la phase d'entrainement"
    
    def after_all_players_arrive(self):
        pass


class Results(Page):
    timeout_seconds = Constants.results_timeout
    timer_text = 'Vous allez commencer la nouvelle partie dans '

    def is_displayed(self):
        return self.round_number/Constants.players_per_group == (Constants.num_period_IP + Constants.period_in_advance) and self.session.config['treatment'][0] == 'vote'

    def vars_for_template(self):
        return {
         'vote' : self.participant.vars['IPtreatment'],
        }   
        
class Sumary(Page):
    timeout_seconds = 30
    timer_text = 'Vous allez passer à la suite dans '
    
    
    def is_displayed(self):
        return self.round_number == Constants.num_rounds


    def vars_for_template(self):
        
        if self.participant.vars['IPtreatment']== "IP":
            royalties_earned = sum([p.royalties_earned for p in self.player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number))])
            royalties_spent = sum([p.royalties_spent for p in self.player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number))])
          
            return {
             'subtreatment' : self.participant.vars['IPtreatment'],
                'royalties_earned': royalties_earned,
                'royalties_spent': -royalties_spent,
            } 
        else:
            return {
             'subtreatment' : self.participant.vars['IPtreatment'],
            }
            
        

page_sequence = [
    Instructions,
    Test,
#    Test_WaitPage,
    Instructions,
    Board,
    Board_WaitPage,
    Instructions_before_vote,
    Vote,
    ResultsVoteWaitPage,
    Results,
    Sumary
]
