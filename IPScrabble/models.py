from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)
import random


author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'IPScrabble'
    players_per_group = 3
    num_rounds = 72 # for experiment 72 becareful : number rounds in this game is incremented when each player plays, should be a multiple of players_per_group
    
    num_period_IP = 10 # for experiment : 10     remettre pour démo prof. 10 * 3 * 2 = 60
    
    period_in_advance = 2 # 2 => 2*3 * 2 = 12 round

    letter_values = {"A": 1,
                     "B": 3,
                     "C": 3,
                     "D": 2,
                     "E": 1,
                     "F": 4,
                     "G": 2,
                     "H": 4,
                     "I": 1,
                     "J": 8,
                     "K": 10,
                     "L": 1,
                     "M": 2,
                     "N": 1,
                     "O": 1,
                     "P": 3,
                     "Q": 8,
                     "R": 1,
                     "S": 1,
                     "T": 1,
                     "U": 1,
                     "V": 4,
                     "W": 10,
                     "X": 10,
                     "Y": 10,
                     "Z": 10}
    
    frequency = ['A'] * 9 + ['B'] * 2 + ['C'] * 2 + ['D'] * 3 + ['E'] * 15 + ['F'] * 2 + ['G'] * 2 + ['H'] * 2 + ['I'] * 8 + ['J'] * 1 + ['K'] * 1 + ['L'] * 5 + ['M'] * 3 + ['N'] * 6 + ['O'] * 6 + ['P'] * 2 + ['Q'] * 1 + ['R'] * 6 + ['S'] * 6 + ['T'] * 6 + ['U'] * 6 + ['V'] * 2 + ['W'] * 1 + ['X'] * 1 + ['Y'] * 1 +  ['Z'] * 1 

    
    initpayoff = c(0)
    letter_price = c(2)
    init_nb_letters = 5
    min_nb_letters = 3
    test_min_nb_letters = 3
    max_nb_letters = 5
    ext_nb_letters = 1
    test_hand = ['A','M','A','D','I','T','E','R','S']
    words = ['TENU','DINA','IDEE','NAGE','SORS','POIL','POSA','LIEU','ORAL','VOUS','NUES','CEDE']






    
    test_timeout = 180
    vote_delay = 20
    board_timeout = 45
    board_timeout_bonus = 75
    board_timeout_bonus_rounds = 2
    board_timeout_bonus_for_royalties = 10
    results_timeout = 25

class Subsession(BaseSubsession):

    def creating_session(self):
        
        
        if self.round_number == 1:
            self.session.vars['subtreatment_round'] = self.round_number
            self.session.vars['vote'] = False
            
            words_temp = random.sample(Constants.words,12)
            self.session.vars['words_in_advance_1'] = words_temp[:6]
            self.session.vars['words_in_advance_2'] = words_temp[6:]
        
            for p in self.get_players():
                p.payoff = Constants.initpayoff
                p.participant.vars['hand'] = []
                
                if self.session.config["standalone"] :
                    p.participant.vars['IPtreatment'] = self.session.config['treatment']
                else:
                    p.participant.vars['IPtreatment'] = 'IP'
        
                           
        if self.round_number/Constants.players_per_group <= Constants.period_in_advance :
            words_in_advance = self.session.vars['words_in_advance_1']
            for p in self.get_players():
                if ((self.round_number - 1) % Constants.players_per_group) + 1 == p.id_in_group:
                    p.new_word = words_in_advance[(self.round_number - 1)%len(words_in_advance)]
                    p.letters_added = p.new_word
                    p.royalties = 50
                    p.init_hand()
                    
        elif (self.round_number/Constants.players_per_group > Constants.period_in_advance  + Constants.num_period_IP and self.round_number/Constants.players_per_group <= 2*Constants.period_in_advance + Constants.num_period_IP):
            words_in_advance = self.session.vars['words_in_advance_2']
            for p in self.get_players():
                if ((self.round_number - 1) % Constants.players_per_group) + 1 == p.id_in_group:
                    p.new_word = words_in_advance[(self.round_number - 1)%len(words_in_advance)]
                    p.letters_added = p.new_word
                    p.royalties = 50
                    p.init_hand()
                
                
    def check_dictionary(self,word):

        dictionary = open("IPScrabble/dic.txt").read().split()
        #Raises an error if the word being played is not in the official scrabble dictionary (dic.txt).
        if word in dictionary:
            return True
        else:
            return False
        
    def get_period(self):
        if not self.session.vars['vote']:
            return (int((self.round_number-1)/Constants.players_per_group)+1) - Constants.period_in_advance
        else:
            return (int((self.round_number-1)/Constants.players_per_group)+1) - (2 * Constants.period_in_advance) - Constants.num_period_IP

    def word_score(word):
        #Calculates the score of a word
        score = 0
        if len(word) > 1 :
            for letter in word:
                score += Constants.letter_values[letter]
        else:
            score = Constants.letter_values[word]
        return c(score)
    
    def get_word_letters_score(word):
        word_list = []
        if len(word) > 1 :
            for letter in word:
                word_list.append([letter,int(Subsession.word_score(letter))])
        else:
            word_list.append([word,Subsession.word_score(word)])
        return word_list
    
    def last_round(round_number):
        if round_number == 1:
            return 1
        else : 
            return round_number - 1

class Group(BaseGroup):

        
    def get_board_hist(self):
        board = []
        for player in self.get_players():
            for p in player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number)):
                if p.new_word != None:
                    board.append(p.new_word) 
        return board
    
    def check_novelty(self,word):
        if word in self.get_board_hist():
            return False
        else:
            return True
        
    def vote_result(self):
        if self.session.config['treatment'][0] == 'vote':
            if sum([p.vote for p in self.get_players()]) == Constants.players_per_group :        
                for player in self.get_players():
                    player.participant.vars['IPtreatment'] = 'IP'
            else:
                for player in self.get_players():
                    player.participant.vars['IPtreatment'] = 'noIP'
        else:
                for player in self.get_players():
                    player.participant.vars['IPtreatment'] = 'noIP'
        
    def reset_hand(self):
        for player in self.get_players():
            player.participant.vars['hand'] = []
            player.init_hand()
            
    def reset_payoff(self):
        for player in self.get_players():
            player.participant.vars['first_session_payoff'] = player.participant.payoff        
            player.participant.payoff = 0
                    
    def word_owners(self,word, players):
        owners = []
        for player in players:
            for p in player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number)):
                if p.new_word == word:
                    owners.append(p)
                    if p.extended_word != None:
                        owners += self.word_owners(p.extended_word, self.get_players())                       
        return owners
            
    
class Player(BasePlayer):
    
    extended_word = models.StringField(label="Mot étendu",blank=True)
    new_word = models.StringField(label="Nouveau mot",blank=True)
    letters_added = models.StringField(label="Lettres ajoutées",blank=True)
    hand = models.StringField()
    timeout = models.BooleanField()    
    vote = models.BooleanField()
    test_words = models.LongStringField()
    royalties_earned = models.CurrencyField(initial=0)
    royalties_spent = models.CurrencyField(initial=0)
    myround = models.BooleanField()

    royalties = models.IntegerField(
    choices=[
        [0, '0 %'],
        [10, '10 %'],
        [20, '20 %'],
        [30, '30 %'],
        [40, '40 %'],
        [50, '50 %'],
        [60, '60 %'],
        [70, '70 %'],
        [80, '80 %'],
        [90, '90 %'],
        [100, '100 %']
    ],
    label="",
    blank=True,
    widget=widgets.RadioSelectHorizontal
)


    def set_board(self): 
        if self.new_word != None:
            self.payoff += Subsession.word_score(self.new_word)

        if self.extended_word != None and self.participant.vars['IPtreatment'] == 'IP':
            owners = self.group.word_owners(self.extended_word, self.group.get_players())
                        
            #distribute royalties to the owners
            for p in owners:
                if p.id_in_group != self.id_in_group: # don't pay yourself 
                    pay_royalties = Subsession.word_score(p.letters_added)*(p.royalties/100)
                    
                    p.in_round(self.round_number).payoff += pay_royalties
                    p.in_round(self.round_number).royalties_earned += pay_royalties                
                    self.payoff -= pay_royalties
                    self.royalties_spent += pay_royalties

    def get_board_hist_players(self):
        board = []
        board_player = []
        for p in self.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number)):
            if p.new_word != None:
                board_player.append(p.new_word)  
        board.append(board_player) 
        
        for other_player in self.get_others_in_group():
            board_player = []
            for p in other_player.in_rounds(self.session.vars['subtreatment_round'], Subsession.last_round(self.round_number)):
                if p.new_word != None:
                    board_player.append(p.new_word)   
            board.append(board_player) 
        return board

    def get_board_word_cost(self):
        board = []
        for player_word in self.get_board_hist_players():
            board_player = []
            for word in player_word:              
                word_cost = self.word_cost(word)
                word_score = Subsession.word_score(word)
                words_for_board = [Subsession.get_word_letters_score(word),word,word_cost*100/word_score,-word_cost,int(word_score)]
                board_player.append(words_for_board)   
            board.append(board_player)
        return board
    
    def word_cost(self,word):
        if self.participant.vars['IPtreatment'] == 'IP':
            cost = 0
            for p in self.group.word_owners(word,self.group.get_players()):       
                if p.royalties != None and p.royalties != 0:
                    if p.id_in_group != self.id_in_group:
                        cost += Subsession.word_score(p.letters_added)*(p.royalties/100)
        else:
            cost = Subsession.word_score(word)
        return cost
    
    def set_hand(self):
        if self.new_word != None:
            for letter in self.letters_added:
                self.remove_from_hand(letter)
                
        if self.round_number == self.session.vars['subtreatment_round']:
            self.participant.vars['new_letter'] == []     
        else:
            self.add_to_hand()
                                
    def add_to_hand(self):
            new_letter = random.choice(Constants.frequency)
            self.participant.vars['new_letter'] = new_letter
        
            self.participant.vars['hand'].append(new_letter)

    def init_hand(self):
        self.participant.vars['new_letter'] = []
        #Adds the initial tiles to the player's hand.
        hand = []
        for i in range(Constants.init_nb_letters):
            hand.append(random.choice(Constants.frequency))
        
        self.participant.vars['hand'] = hand

#        self.hand = ''.join(hand)

    def remove_from_hand(self, tile):
        #Removes a tile from the hand (for example, when a tile is being played).
        self.participant.vars['hand'].remove(tile)

    def get_hand_length(self):
        #Returns the number of tiles left in the hand.
        return len(self.participant.vars['hand'])

    def get_new_letter(self): 
        if self.participant.vars['new_letter'] != None:
            return self.participant.vars['new_letter']
        else:
            return None
