from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants, Subsession

# In my_app.pages
from django.http import HttpResponse

def dictionary(request):
   
    word = request.POST.get('word')
    dictionary = open("words_task/dic.txt").read().split()
    #Raises an error if the word being played is not in the official scrabble dictionary (dic.txt).
    if word in dictionary:
        exist = True
    else:
        exist = False
    return HttpResponse(exist)

class Instructions(Page):    
    def is_displayed(self):
        return self.round_number == 1

class Test(Page):
    timeout_seconds = Constants.test_timeout
    
    def is_displayed(self):
        return self.round_number == 1

    form_model = 'player'

    def get_form_fields(self):
        return ['test_words']     

    def vars_for_template(self):
        return {
            'test_hand': Constants.test_hand,
            'test_timeout' : int(Constants.test_timeout/60) #display in minutes
        }
    
    
class Test_WaitPage(WaitPage):

    def is_displayed(self):
        return self.round_number == 1
    
    title_text = "Veuillez patienter"
    body_text = "En attendant que les autres participants finissent la phase d'entrainement"
    
    def after_all_players_arrive(self):
        pass
            
        

page_sequence = [
#    Instructions,
    Test,
    Test_WaitPage,
]
