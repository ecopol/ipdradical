from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)
import random


author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'words_task'
    players_per_group = None
    num_rounds = 1

    letter_values = {"A": 1,
                     "B": 3,
                     "C": 3,
                     "D": 2,
                     "E": 1,
                     "F": 4,
                     "G": 2,
                     "H": 4,
                     "I": 1,
                     "J": 8,
                     "K": 10,
                     "L": 1,
                     "M": 2,
                     "N": 1,
                     "O": 1,
                     "P": 3,
                     "Q": 8,
                     "R": 1,
                     "S": 1,
                     "T": 1,
                     "U": 1,
                     "V": 4,
                     "W": 10,
                     "X": 10,
                     "Y": 10,
                     "Z": 10}
    
    frequency = ['A'] * 9 + ['B'] * 2 + ['C'] * 2 + ['D'] * 3 + ['E'] * 15 + ['F'] * 2 + ['G'] * 2 + ['H'] * 2 + ['I'] * 8 + ['J'] * 1 + ['K'] * 1 + ['L'] * 5 + ['M'] * 3 + ['N'] * 6 + ['O'] * 6 + ['P'] * 2 + ['Q'] * 1 + ['R'] * 6 + ['S'] * 6 + ['T'] * 6 + ['U'] * 6 + ['V'] * 2 + ['W'] * 1 + ['X'] * 1 + ['Y'] * 1 +  ['Z'] * 1 

    
    initpayoff = c(0)
    letter_price = c(2)
    init_nb_letters = 5
    min_nb_letters = 3
    test_min_nb_letters = 3
    max_nb_letters = 5
    ext_nb_letters = 1
    test_hand = ['A','M','A','D','I','T','E','R','S']
    
    test_timeout = 180


class Subsession(BaseSubsession):
    pass

class Group(BaseGroup):
    pass      
    
class Player(BasePlayer):    
    test_words = models.LongStringField()
