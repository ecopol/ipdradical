# urls.py
from django.conf.urls import url
from otree.urls import urlpatterns
from IPScrabble.pages import dictionary


urlpatterns.append(url(r'^dictionary/$', dictionary))
