from ._builtin import Page, WaitPage
from otree.api import Currency as c, currency_range
from .models import Constants



       
class Introduction(Page):
    
    def is_displayed(self):
        return self.round_number ==1  

class WaitBeforeStart(WaitPage):
    
    def is_displayed(self):
        return self.round_number ==1

    wait_for_all_groups = True
    
    def after_all_players_arrive(self):
        self.subsession.set_init_payoff()
        self.subsession.do_ranking()
        self.subsession.set_weather()
        for subsession in self.subsession.in_rounds(2, Constants.num_rounds):
            subsession.group_like_round(1)  
        self.subsession.init_graph_report()

class Ranking(Page):
    template_name = 'IPDradical/Ranking.html'

    timeout_seconds = 45
    timer_text = 'Début de la partie dans '
    
    def is_displayed(self):
        return self.round_number ==1

    def vars_for_template(self):
        
        me = self.player
        if not self.session.config["standalone"]:                
            multiplier = self.session.config["IPD_payoff_multiplier"]
        else:
            multiplier = 1
            
        return {
            'my_role': me.role(),
            'my_colors': Constants.colors[me.role()],
            'colorsname' : Constants.colorsname,
            'mycolorname' : Constants.colorsname[me.role()],
            'session_choice': self.participant.vars['session_payoff_choice'],
            'multiplier': multiplier,
            'colors': Constants.colors,
            'style_colors' : ["color:" + s for s in Constants.colors],
            'standalone': self.session.config["standalone"],
            'players': ["color:" + Constants.colors[p.role()] for p in self.group.get_players()],


        }

class Decision(Page):
    
    timer_text = 'Temps restant pour faire votre choix '
    
    def get_timeout_seconds(self):
        if self.round_number <= Constants.decision_timeout_rounds :
            return Constants.decision_timeout + Constants.decision_timeout_bonus
        else:
            return Constants.decision_timeout
 
    def is_displayed(self):#display page Decision only if not dead and not alone
        nb_alive=0
        for x in range(Constants.nb_group):
            if self.group.get_total_payoff_role(x) > 0: #if group not dead
                nb_alive += 1
        return (self.group.get_total_payoff_role(self.player.role()) >= 1 and nb_alive > 1 and self.player.participant.payoff >= 1)
    
    form_model = 'player'

    def before_next_page(self):
        self.player.weather = self.participant.vars['weather']
        
        if self.timeout_happened:
            self.player.timeout = True
            
            random_choice = self.player.random_choice()
            
            self.player.group_0 = random_choice[0]
            self.player.group_1 = random_choice[1]
            self.player.group_2 = random_choice[2]
            self.player.group_3 = random_choice[3]
            self.player.group_4 = random_choice[4]
    
    def get_form_fields(self):
        me = self.player
        return me.list_form_fields_group()

    def vars_for_template(self):

        
        me = self.player
        
        return {
            'my_role': me.role(),
            'nb_group': self.group.nb_group_inlive(),
            'groups': self.get_form_fields(),
            'roles_list': me.get_random_roles_list(),
            'initial_payoff': me.participant.payoff,
            'round': self.round_number,
            'lastround': self.round_number - 1,
            'round_list': list(range(1, Constants.num_rounds + 1)),
            'highcharts_series': self.session.vars['group_evolution'],
            'highcharts_series_columns':me.graph_report_columns(),
            'colors': Constants.colors,
            'style_colors' : ["color:" + s for s in Constants.colors],
            'my_colors': Constants.colors[me.role()],
            'huerotate': Constants.huerotate,
            'weather': self.participant.vars['weather'],
            'weather_val_percent': self.group.weather_val_percent(),
            'weather_next_tour': (Constants.interval - self.round_number) % Constants.interval,
            'max_attack': self.player.max_attack(),
            'max_attack_percent':self.player.max_attack_percent(),
            'group_payoff': Constants.group_attack_payoff * (Constants.players_per_role),
            'individualist_payoff': Constants.individualism_attack_payoff,
            'group_attacked_payoff': abs(Constants.attacked_payoff * (Constants.players_per_role)),
            'colorsname' : Constants.colorsname,
            'mycolorname' : Constants.colorsname[me.role()],
            'mode': self.session.config['treatment'][2] #parochial or individualist

        }



class ResultsWaitPage(WaitPage):
    
    title_text = "Veuillez patienter en attendant que les autres participants finissent de jouer"

    template_name = 'IPDradical/ResultsWaitPage.html'

    def after_all_players_arrive(self):
        self.group.set_payoffs()
        self.subsession.set_graph_report()
        if self.round_number % Constants.interval == 0:
            self.subsession.set_weather()

    def vars_for_template(self):

        
        me = self.player
        
        return {
            'my_role': me.role(),
            'round': self.round_number,
            'lastround': self.round_number - 1,
            'round_list': list(range(1, Constants.num_rounds + 1)),
            'highcharts_series': self.session.vars['group_evolution'],
            'highcharts_series_columns':me.graph_report_columns(),
            'colors': Constants.colors,
            'style_colors' : ["color:" + s for s in Constants.colors],
            'my_colors': Constants.colors[me.role()],
            'huerotate': Constants.huerotate,
            'weather': self.participant.vars['weather'],
            'weather_val_percent': self.group.weather_val_percent(),
            'weather_next_tour': (Constants.interval - self.round_number) % Constants.interval,
            'colorsname' : Constants.colorsname,
            'mycolorname' : Constants.colorsname[me.role()]

        }

class Weather(Page):
    timeout_seconds = 20

    def is_displayed(self):##display page each Constants.interval ex. each 5 rounds and display page Decision only if not dead and not alone
        nb_alive=0
        for x in range(Constants.nb_group):
            if self.group.get_total_payoff_role(x) > 0: #if group not dead
                nb_alive += 1
        return ((self.round_number-1) % Constants.interval == 0) and (self.group.get_total_payoff_role(self.player.role()) >= 1 and nb_alive > 1 and self.player.participant.payoff >= 1)
    
    def before_next_page(self):
        if self.timeout_happened:
            self.player.weather = self.participant.vars['weather']
    
    def vars_for_template(self):
        
        self.player.weather = self.participant.vars['weather']
        
        if not self.session.config["standalone"]:                
            multiplier = self.session.config["IPD_payoff_multiplier"]
        else:
            multiplier = 1
        
        me = self.player
                
        return {
            'my_role': me.role(),
            'nb_group': self.group.nb_group_inlive(),
            'groups': self.get_form_fields(),
            'roles_list': me.get_random_roles_list(),
            'round': self.round_number,
            'lastround': self.round_number - 1,
            'round_list': list(range(1, Constants.num_rounds + 1)),
            'colors': Constants.colors,
            'style_colors' : ["color:" + s for s in Constants.colors],
            'my_colors': Constants.colors[me.role()],
            'huerotate': Constants.huerotate,
            'weather': me.weather,
            'weather_val_percent': self.group.weather_val_percent(),
            'colorsname' : Constants.colorsname,
            'mycolorname' : Constants.colorsname[me.role()],
            'session_choice': self.participant.vars['session_payoff_choice'],
            'multiplier': multiplier,
            'standalone': self.session.config["standalone"]

        }        
        
class Results(Page):
    timer_text = 'Prochain tour dans '

    def get_timeout_seconds(self):
        if self.round_number <= Constants.results_timeout_rounds :
            return Constants.results_timeout + Constants.results_timeout_bonus
        else:
            return Constants.results_timeout

    def is_displayed(self):#display page Decision only if not dead and not alone
        nb_alive=0
        for x in range(Constants.nb_group):
            if self.group.get_total_payoff_role(x) > 0: #if group not dead
                nb_alive += 1
        return (self.group.get_total_payoff_role(self.player.role()) >= 1 and nb_alive > 1 and self.player.participant.payoff >= 1)

    
    def vars_for_template(self):                   
        me = self.player   
                        
        my_parteners_attacks = me.my_parteners_attacks()
        
        if self.session.config['treatment'][2] == 'parochial':
            my_attacks = me.total_attack()*Constants.group_attack_payoff*Constants.players_per_role
        else:
            my_attacks = me.total_attack()*Constants.individualism_attack_payoff            

        weather_points = c(round( Constants.weather_value.get(me.weather)*sum([p.participant.vars['payoff_before_weather'] for p in self.subsession.get_players_by_role(me.role())])*2) / 2)

        highcharts_series_results = self.group.graph_results(me.role())
        
        highcharts_series_results.append(
                                        {
                                        'type': 'column',
                                        'color': Constants.colors[me.role()],
                                        'data': ['',my_attacks, my_parteners_attacks[0],my_parteners_attacks[1],weather_points ]
                                        })
            
#        highcharts_series_results.append(
#                                        {
#                                        'type': 'column',
#                                        'name': 'Vous',
#                                        'color': '#c0c0c0',
#                                        'data': ['','','','',  me.payoff]
#                                        })
        return {
            'my_role': me.role(),
            'nb_group': self.group.nb_group_inlive(),
            'groups': self.get_form_fields(),
            'roles_list': me.get_random_roles_list(),
            'round': self.round_number,
            'initial_payoff': me.participant.vars['payoff_before_attack'],
            'lastround': self.round_number - 1,
            'round_list': list(range(1, Constants.num_rounds + 1)),
            'colors': Constants.colors,
            'style_colors' : ["color:" + s for s in Constants.colors],
            'my_colors': Constants.colors[me.role()],
            'huerotate': Constants.huerotate,
            'highcharts_series_results': highcharts_series_results,
            'highcharts_series': self.session.vars['group_evolution'],
            'highcharts_series_columns':me.graph_report_columns(),
            'weather': me.weather,
            'myweather_percent' : Constants.weather_value[me.weather]*100,
            'weather_points' : weather_points,
            'weather_val_percent': self.group.weather_val_percent(),
            'weather_next_tour': (Constants.interval - self.round_number) % Constants.interval,
            'colorsname' : Constants.colorsname,
            'mycolorname' : Constants.colorsname[me.role()],
            'my_group_attacks' : my_parteners_attacks,
            'my_attacks' : my_attacks,
            'mode': self.session.config['treatment'][2] #parochial or individualist
        }


page_sequence = [
    Introduction,
    WaitBeforeStart,
    Ranking,
    Weather,
    Decision,
    ResultsWaitPage,
    Results
]
