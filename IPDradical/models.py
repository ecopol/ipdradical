from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)

doc = """
This is an inter-group social class prisoner dilemna.
"""
import random
import time


class Constants(BaseConstants):
    name_in_url = 'IPDradical'
    players_per_group = 15 #all player in same group
    
    nb_group = 5 # If you change, do not forget to add more variable field in player like group_i = make_field(i)
    
    players_per_role = 3

    num_rounds = 15 # in exp : 20
         
    instructions_template = 'IPDradical/Instructions.html'
    
    colorsname = ['Jaune', 'Rouge', 'Violet', 'Bleu', 'Vert']
    colors = ['#ffc107','#ff816c','#d496ff','#8cc5fb','#83eb64'] #jaune, rouge, bleu,vert
    huerotate = [0,310,200,180,80]

    attacked_payoff = c(-0.5)
    player_attack_cost = c(-1.0)
    group_attack_payoff = c(0.5)
    individualism_attack_payoff = c(1.5)
    
    absolute_limit_attack = c(50)
    relative_limit_attack = 0.1 # number between [0,1]
    
    init_payoffs = [400,500,600,700,800,
                    400,500,600,700,800,
                    400,500,600,700,800,
                    ]
    
    weather_value = {
            "negative":-0.02,
            "neutral":0.02,
            "positive":0.06
            }

    interval = 5
    
    
    decision_timeout_rounds = 2
    decision_timeout = 45
    decision_timeout_bonus = 75
    
    results_timeout_rounds = 2
    results_timeout = 45
    results_timeout_bonus = 75

class Subsession(BaseSubsession):
    
    def creating_session(self):
        
        if self.round_number == 1:
            for player in self.get_players():
                list_group = list(range(Constants.nb_group))
                random.shuffle(list_group)
                player.participant.vars['rand_display_group'] = list_group

                if self.session.config["standalone"]:
                    player.participant.payoff = c(Constants.init_payoffs[player.id_in_group-1])
                    player.participant.vars['initial_payoff'] = c(Constants.init_payoffs[player.id_in_group-1])
                    player.participant.vars['session_payoff_choice'] = 1

            
    def set_init_payoff(self):
        if not self.session.config["standalone"]:
            choice = random.randint(0,1)
            for player in self.get_players():
                player.participant.vars['session_payoff_choice'] = choice
                sessions_payoff = [player.participant.vars['first_session_payoff'],player.participant.payoff]
                player.participant.payoff = sessions_payoff[choice] * self.session.config["IPD_payoff_multiplier"] 
                player.participant.vars['initial_payoff'] = player.participant.payoff
            
    def do_ranking(self):

        players = self.get_players()
        
        rank = sorted(players, key=lambda players: players.participant.payoff, reverse=True) # list all players by desc payoff

        self.set_group_matrix([rank])
        
    def get_players_by_role(self, role):
        return [p for p in self.get_players() if p.role() == role]

    def set_weather(self):#for the next tour
        for role in range(Constants.nb_group):
            role_weather = random.choice(list(Constants.weather_value.keys()))
            for player in self.get_players_by_role(role):
                player.participant.vars['weather'] = role_weather

 

    def vars_for_admin_report(self):       

        
        return {
            'nb_group': Constants.nb_group,
            'groups': ["group_" + format(s) for s in range(Constants.nb_group)],
            'roles_list': list(range(Constants.nb_group)),
            'round': self.round_number,
            'round_list': list(range(1, Constants.num_rounds + 1)),
            'highcharts_series': self.session.vars['group_evolution'],
            'highcharts_series_columns': self.graph_report_columns(0),
            'colors': Constants.colors,
            'style_colors' : ["color:" + s for s in Constants.colors],
        }        

    def init_graph_report(self):    
        group = self.get_groups()[0]

        self.session.vars['group_evolution'] = []            
        series = self.session.vars['group_evolution']

        for role in range(Constants.nb_group):     
        
            payoffs = 0                        
            for player in group.get_players_by_role(role):#for all players in the team
                payoffs += player.participant.payoff

            if self.round_number == 1:                
                series.append(
                    {'name': 'Cagnotte du groupe {}'.format(role),
                     'group': role,
                     'data': [payoffs],
                     'color': Constants.colors[role],
                     'colorIndex':role,
                     })  
            
    def set_graph_report(self):
        group = self.get_groups()[0]

        series = self.session.vars['group_evolution']

        for role in range(Constants.nb_group):     
        
            payoffs = 0                        
            for player in group.get_players_by_role(role):#for all players in the team
                payoffs += player.participant.payoff

            series[role]['data'].append(payoffs)   

class Group(BaseGroup):
    
    
    def get_roles_list(self,role):
        #list all the group by order : my group first, then the others group
        roleslist = []
        roleslist.insert(0, role)
        for x in range(Constants.nb_group):
            if x != role and self.get_total_payoff_role(x) > 0: #if group not dead
                roleslist.append(x)
        return roleslist   
    
    def get_players_by_role(self, role):
        return [p for p in self.get_players() if p.role() == role]
    
    def get_players_by_other_role(self, role):
        return [p for p in self.get_players() if p.role() != role]

    def graph_results(self,role):
        
        group_attacks_me = []
        for others_role in range(Constants.nb_group):
            if others_role != role:
                group_attacks_me.append(
                            {'name': Constants.colorsname[others_role],
                             'data': [sum([Constants.attacked_payoff*p.box(role)*Constants.players_per_role for p in self.get_players_by_role(others_role)])],
                             'color': Constants.colors[others_role],
                             })       
        return group_attacks_me  
        
    def other_group_attacks(self, role):
        group_attacks_me = []
        for others_role in range(Constants.nb_group):
            if others_role != role:
                group_attacks_me.append([Constants.colorsname[others_role],sum([Constants.attacked_payoff*p.box(role) for p in self.get_players_by_role(others_role)])])
                
        return group_attacks_me
        
    def get_total_payoff_role(self,role):
        total = 0
        for player_role_ingroup in self.get_players_by_role(role):#for all players in the team
            total = total + player_role_ingroup.participant.payoff  
        return total    
    
    def nb_group_inlive(self):
        nb=0
        for role in range(Constants.nb_group):
            if self.get_total_payoff_role(role) > 0:
                nb = nb + 1
        return nb

    def group_alive(self):
        group = []
        for role in range(Constants.nb_group):
            if self.get_total_payoff_role(role) > 0:
                group.append(role)
        return group

    def weather_val_percent(self):       
        return {key: 100 * Constants.weather_value[key] for key in Constants.weather_value.keys()}
                    
    def set_payoffs(self):
                 
        #loop over all the players to set there payoffs after game tour

        for player in self.get_players():
            player.participant.vars['payoff_before_attack'] = player.participant.payoff
       
        for player in self.get_players():

            player.payoff += ( player.total_attack() * Constants.player_attack_cost ) # cost of attack this tour for the player
 
            for player_role_attacked in self.get_players_by_other_role(player.role()):#for players attacked by player
            
                player_role_attacked.payoff += ( player.box(player_role_attacked.role()) * Constants.attacked_payoff )

            if self.session.config['treatment'][2] == 'parochial':
                
                for player_role_ingroup in self.get_players_by_role(player.role()):#for all players in the team
    
                    player_role_ingroup.payoff += ( player.total_attack() * Constants.group_attack_payoff )
            else: 
                player.payoff += ( player.total_attack() * Constants.individualism_attack_payoff  ) 
         
        for player in self.get_players():
             player.participant.vars['payoff_before_weather'] = player.participant.payoff 
            
             player.payoff += round(player.participant.payoff * Constants.weather_value.get(player.participant.vars['weather'])*2) / 2 # weather and round the result
             
             if player.participant.payoff < 0 :
                player.participant.payoff = 0 

class Player(BasePlayer): 
        
#    def get_other_group_number(self):
#        me = self.player
#        #list all the group by order : my group first, then the others group
#        grouplist = []
#        grouplist.insert(0, 'group_{}'.format(me.role()))
#        for x in range(Constants.nb_group):
#            if x != me.role():
#                grouplist.append('group_{}'.format(x))
#        return grouplist


    weather = models.StringField()
    timeout = models.BooleanField()
            
    def make_field(group):
        return models.CurrencyField(
            name='group_{}'.format(group),
            label='{}'.format(group),

        )


# If you want more groups, add a variable for each new group i group_i = make_field(i) :
    # do not forget to change also Constants.nb_group    

    group_0 = make_field(0)
    group_1 = make_field(1)
    group_2 = make_field(2)
    group_3 = make_field(3)
    group_4 = make_field(4)
    
#    group_5 = make_field(5)
#    group_6 = make_field(6)
#    group_7 = make_field(7)

# If you want more groups, add a variable for each new group i  if group_box == i: return self.group_i
    # do not forget to change also Constants.nb_group  
    
    def box(self,group_box):

        if group_box == 0 :
            if self.group_0 is None :
                return 0
            else:
                return self.group_0
        if group_box == 1:
            if self.group_1 is None :
                return 0
            else:
                return self.group_1
        if group_box == 2:
            if self.group_2 is None :
                return 0
            else:
                return self.group_2
        if group_box == 3:
            if self.group_3 is None :
                return 0
            else:
                return self.group_3
        if group_box == 4:
            if self.group_4 is None :
                return 0
            else:
                return self.group_4
        
#        if group_box == 5:
#            return self.group_5
#        if group_box == 6:
#            return self.group_6
#        if group_box == 7:
#            return self.group_7



    def random_choice(self):
        choice = [0]*Constants.nb_group
        
        attack = random.randint(0,self.max_attack())
        if attack > 0 :
            for role in self.group.group_alive():
                if role != self.role():
                    choice[role] = random.randint(0,attack)
            
            if sum(choice) > 0:
                choice = [c(round(i*attack/sum(choice))) for i in choice]
        
        choice[self.role()] = self.participant.payoff - sum(choice)
        return choice

   
    def role(self):
        
        return int((self.id_in_group-1)/3)
        
        
    def total_attack(self): # Compute the sum of the attack for the player
        total = 0
        for b in range(Constants.nb_group):
            if b != self.role() :
                if self.box(b) is not None :
                    total = total + self.box(b)
        return total

    def initial_payoff(self,round_number):
        if round_number > 1 :
            return self.in_round(round_number - 1).participant.payoff
        else :
            return self.participant.vars['initial_payoff']

    def max_attack(self):
        if self.session.config['treatment'][1] == 'absolute':
            if Constants.absolute_limit_attack > self.participant.payoff :
                return c(round(self.participant.payoff))
            else:
                return c(round(Constants.absolute_limit_attack))
        else:
            return c(round(Constants.relative_limit_attack*self.participant.payoff))

    def max_attack_percent(self):
        if self.session.config['treatment'][1] == 'absolute':
            if Constants.absolute_limit_attack > self.participant.payoff :
                return 100
            else:
                return round(100*Constants.absolute_limit_attack/self.participant.payoff)
        else:
            if 100*Constants.relative_limit_attack > self.participant.payoff :
                return 0
            else:
                return round(100*Constants.relative_limit_attack)      

    def my_parteners_attacks(self):
        if self.session.config['treatment'][2] == 'parochial':            
            return [p.total_attack()*(Constants.group_attack_payoff*Constants.players_per_role) for p in self.subsession.get_players_by_role(self.role()) if p.id_in_group != self.id_in_group]
        else:
            return [p.total_attack()*(Constants.individualism_attack_payoff) for p in self.subsession.get_players_by_role(self.role()) if p.id_in_group != self.id_in_group]
        
    def get_random_roles_list(self):
        #list all the group by order : my group first, then the others group
        roleslist = []
        role = self.role()
        roleslist.insert(0, role)
        for x in self.participant.vars['rand_display_group']:
            if x != role and self.group.get_total_payoff_role(x) > 0: #if group not dead
                roleslist.append(x)
        return roleslist 
                          
    def list_form_fields_group(self):
        # form name
        return ['group_{}'.format(x) for x in self.get_random_roles_list()]   
    
    def graph_report_columns(self):
        myrole = self.role()
        
        series = []
        
        roleslist = []
        roleslist.insert(0, myrole)
        for x in self.participant.vars['rand_display_group']:
            if x != myrole : 
                roleslist.append(x)

        for role in roleslist:
            payoffs_of_group = []
            payoffs = 0
            for player in self.group.get_players_by_role(role):#for all players in the team
                payoffs += player.participant.payoff

            if payoffs >=1:                    
                payoffs_of_group.append(payoffs)
                series.append(
                    {'name': 'Cagnotte du groupe {}'.format(role),
                     'group': role,
                     'data': payoffs_of_group,
                     'color': Constants.colors[role],
                     'colorIndex':role,
                     })
            

        return series 