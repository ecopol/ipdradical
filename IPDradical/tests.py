from otree.api import Currency as c, currency_range
from . import pages
from ._builtin import Bot
from .models import Constants

class PlayerBot(Bot):
    
    def play_round(self):
#        yield (pages.Introduction)

        nb_alive=0
        for x in range(Constants.nb_group):
            if self.group.get_total_payoff_role(x) > 0: #if group not dead
                nb_alive += 1
        if (self.group.get_total_payoff_role(self.player.role()) > 0 and nb_alive > 1):
    
        # Players of the group 0
            if self.player.id_in_group == 4:
                yield (pages.Decision, self.attack(1,1,1))
            if self.player.id_in_group == 8:
                yield (pages.Decision, self.attack(0,0,0))
            if self.player.id_in_group == 12:
                yield (pages.Decision, self.attack(0,0,0))
    
    # Players of the group 1     
            if self.player.id_in_group == 1:
                yield (pages.Decision, self.attack(0,0,0))
            if self.player.id_in_group == 5:
                yield (pages.Decision, self.attack(0,0,0))
            if self.player.id_in_group == 9:
                yield (pages.Decision, self.attack(0,0,0))
    
    # Players of the group 2               
            if self.player.id_in_group == 2:
                yield (pages.Decision, self.attack(0,0,0))
            if self.player.id_in_group == 6:
                yield (pages.Decision, self.attack(0,0,0))
            if self.player.id_in_group == 10:
                yield (pages.Decision, self.attack(0,0,0))
    
    # Players of the group 3                          
            if self.player.id_in_group == 3:
                yield (pages.Decision, self.attack(0,0,0))
            if self.player.id_in_group == 7:
                yield (pages.Decision, self.attack(0,0,0))
            if self.player.id_in_group == 11:
                yield (pages.Decision, self.attack(0,0,0))

        print("id:",self.player.id_in_group," round:",self.round_number," role: ", self.player.role(), "payoff:",self.player.payoff)            
        yield (pages.Results)
   
    def attack(self,x,y,z):
        
        
        if self.round_number > 1:
            my_payoff = self.player.in_round(self.round_number - 1).payoff
        else :
            my_payoff = self.player.payoff

        list_role = self.group.list_form_fields_group(self.player.role())
        box = [my_payoff - (x+y+z),c(x),c(y),c(z)]
        
        attacklist = dict({})
        x=0
        for role_field in list_role:
            attacklist[role_field] = box[x]
            x += 1
            
        return attacklist