function c(tokens){
        if (tokens > 1){
             return tokens+" points";   
                
                }else{
                        
          return tokens+" point";                 
                        
                        }
        
        
        }

function draw_linechart(data,title, categories, max_round){
       Highcharts.chart('chart', {
            chart: {
            type: 'line'
    },
            credits: {
      enabled: false
  },
            title: {
                text: title
            },
            xAxis: {
                labels: {
                         enabled:false },
                categories: categories,
                title: {text: ''},
                max: max_round,
            },
            yAxis: {
                
            title :{text: 'Nombre de points par groupe'},
            min: 0,

            
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                        
                line: {
            dataLabels: {
                enabled: false
            },
            marker: {
            enabled: true
        },
            enableMouseTracking: false
        }
    },
            series: data
        });   

}

function draw_columnchart(data,title){
 
       Highcharts.chart('chart_column', {
            chart: {
            type: 'column'
    },
            credits: {
      enabled: false
  },
            title: {
                text: title,
            },
            xAxis: {
                categories: [' '],
                        title: {
                            text: ''
                        },
                labels: {
                         enabled:false },
            },
            yAxis: {
                
            title :{text: null},
            min:0,
            labels: {
                enabled: false
                }

            },
            legend: {
                enabled : false,
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            plotOptions: {
                        
                column: {
            dataLabels: {
                enabled: true
            },
            marker: {
            enabled: true
        },
            enableMouseTracking: false
        }
    },
            series: data
        });     
}  

function draw_resultschart(data,title,cat){
     Highcharts.chart('results', {
            chart: {
            type: 'column'
    },
            credits: {
      enabled: false
  },
            title: {
                text: title,
            },
            xAxis: {
                categories: cat,
                        title: {
                            text: ''
                        },
              /*  lineColor: '#000000',
                lineWidth: 5,*/
                labels: {
                         enabled:true,
                         style: {
                              fontSize :'18px',
                              fontWeight: 'normal',
                            }
                         
                         },
                        opposite:true,
            },
            yAxis: {
               
                title: {
                    text: null
                },
            plotLines: [{
                    value: 0,
                    color: '#dc3545',
                    width: 10,
                    zIndex: 2,

                }],            
            labels: {
                enabled: true
                },
                
            },
            legend: {
                enabled : false,
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            plotOptions: {
                        
                column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                style: {
                      fontSize :'18px',
                      fontWeight: 'bold',
                            }
            },
            marker: {
            enabled: true
        },
            enableMouseTracking: false
        }
    },
            series: data
        });     
}